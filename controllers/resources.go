package controllers

import (
	"bitbucket.org/opay-team/ng-hackathon-team-1/models"
)

type (
	//For Post - /user/register
	UserResource struct {
		Data models.User `json:"data"`
	}

	//For Post - /user/login
	TrxLogResource struct {
		Data models.TrxLog `json:"data"`
	}
)
