package controllers

import (
	"encoding/json"
	"net/http"
	"bitbucket.org/opay-team/ng-hackathon-team-1/models"
	"bitbucket.org/opay-team/ng-hackathon-team-1/service"
	"strings"
	"strconv"
)

func writeResponse(w http.ResponseWriter, message string)  {

}

func Action(w http.ResponseWriter, r *http.Request) {

	reqQuery := models.RequestQuery{}

	json.NewDecoder(r.Body).Decode(&reqQuery)

	phoneNumber := reqQuery.Phone
	rawWCode := reqQuery.Code[1:4]


	if rawWCode == "101" {
		pin := reqQuery.Code[5:9]
		err := service.CreateAccount(phoneNumber, pin)

		if err != nil {
			RespondwithJSON(w, http.StatusInternalServerError, map[string]string{"message": err.Error()})
		}

		RespondwithJSON(w, http.StatusOK, map[string]string{"response": rawWCode + "  " + pin})
	}else if rawWCode == "102" {

		parameters := strings.Split(reqQuery.Code, "*")

		receipientPhone := parameters[2]
		pin := parameters[3]
		amount, _ := strconv.ParseFloat(parameters[4], 64)


		sendMoneyInput := service.SendMoneyInput{
			phoneNumber,
			receipientPhone,
			amount,
		pin,
		}

		err := service.SendMoney(sendMoneyInput)

		if err != nil {
			RespondwithJSON(w, http.StatusInternalServerError, map[string]string{"message": "Error sending money"})
		}

		RespondwithJSON(w, http.StatusOK, map[string]string{"response": "Money transfered successfully"})
	} else if  {

	}



}

func RespondwithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}


