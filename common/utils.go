package common

import (
	"encoding/json"
	"net/http"
)

var environments = map[string]string{
	"prod": "common/config/prod.json",
	"dev":  "common/config/dev.json",
	"test": "../common/config/test.json",
}

var env = "dev"

type configuration struct {
	Server, MongoDBHost, DBUser, DBPwd, Database, PrivateKeyPath, PublicKeyPath string
	LogLevel                                                                    int
}

type (
	appError struct {
		Error      string `json:"error"`
		Message    string `json:"message"`
		HttpStatus int    `json:"http_status"`
	}
	appInfo struct {
		Message string `json:"message"`
	}
	errorResource struct {
		Data appError `json:"data"`
	}
)

//App config holds the configuration values from the config.json file
var AppConfig configuration

//Initialize AppConfig
func initConfig() {
	loadAppConfig()
}

//Reads config.json and decode into Appconfig
func loadAppConfig() {
	AppConfig = configuration{
		MongoDBHost: "127.0.0.1:27017",
		Database: "paydb",
		Server: "127.0.0.1:8080",
	}
}

func DisplayAppError(w http.ResponseWriter, handlerError error, message string, code int) {
	errObj := appError{
		Error:      handlerError.Error(),
		Message:    message,
		HttpStatus: code,
	}

	LogAppError(handlerError)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	if j, err := json.Marshal(errorResource{Data: errObj}); err == nil {
		w.Write(j)
	}
}

func LogAppInfo(message interface{}) {
	Info.Printf("[AppInfo]: %s\n", message)
}

func LogAppError(err error) {
	Error.Printf("[AppError]: %s\n", err)
}

func GetEnvironment() string {
	return env
}

func IsTestEnvironment() bool {
	return env == "tests"
}
