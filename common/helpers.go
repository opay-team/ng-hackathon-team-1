package common

import "time"

// Bool is a helper routine that allocates a new bool value
// to store value and returns a pointer to it.
func Bool(value bool) *bool { return &value }

// Int is a helper routine that allocates a new int value
// to store value and returns a pointer to it.
func Int(value int) *int { return &value }

// String is a helper routine that allocates a new string value
// to store value and returns a pointer to it.
func String(value string) *string { return &value }

// Duration is a helper routine that allocates a new time.Duration value
// to store value and returns a pointer to it.
func Duration(value time.Duration) *time.Duration { return &value }
