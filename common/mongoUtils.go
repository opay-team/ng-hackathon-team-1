package common

import (
	"gopkg.in/mgo.v2"
	"log"
	"time"
)

var session *mgo.Session

func GetSession() *mgo.Session {
	if session == nil {
		var err error
		session, err = mgo.DialWithInfo(&mgo.DialInfo{
			Addrs:   []string{AppConfig.MongoDBHost},
			Timeout: 60 * time.Second,
		})
		if err != nil {
			LogAppError(err)
			log.Fatalf("[GetSession]: %s\n", err)
		}
	}
	return session
}

func createDbSession() {
	var err error
	session, err = mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:   []string{AppConfig.MongoDBHost},
		Timeout: 60 * time.Second,
	})
	if err != nil {
		LogAppError(err)
		log.Fatalf("[CreateDbSession]: %s\n", err)
	}
}

func addIndexes() {
	var err error
	userIndex := mgo.Index{
		Key:        []string{"phone_number"},
		Unique:     true,
		Background: true,
		Sparse:     true,
	}

	trxLogIndex := mgo.Index{
		Key:        []string{"id"},
		Unique:     true,
		Background: true,
		Sparse:     true,
	}

	//Add Indexes into MongoDB
	session := GetSession().Copy()
	defer session.Close()
	userCol := session.DB(AppConfig.Database).C("users")
	err = userCol.EnsureIndex(userIndex)
	if err != nil {
		log.Fatalf("[addIndexes]: %s\n", err)
	}

	trxLog := session.DB(AppConfig.Database).C("trx_log")
	err = trxLog.EnsureIndex(trxLogIndex)
	if err != nil {
		log.Fatalf("[addIndexes]: %s\n", err)
	}
}
