$(document).ready(function () {

    var waitBtn = "<i class='fa fa-spin fa-cog'> </i> Please Wait.....";

    $(document).on('submit','.start-process',function(){
      var _this = $(this);
      var _url = _this.attr("action");
        var _submitBtn = _this.find("button[type='submit']");
        _submitBtn.html('please wait').attr('disabled','disabled');
       $.ajax({
          url:_url,
          type:"post",
           dataType:"json",
           data:{"phone":$(".phone-input").val(),"code":$(".code-input").val()},
          success:function(data){
              $(".response-text").text(data);
          },
          error:function(data) {
              $(".response-text").text(data);
          },
          complete:function() {
              $(".form-control").val('');
            _submitBtn.html("Proceed").removeAttr("disabled");
          }
       });
       return false;
    });
});