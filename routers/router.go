package routers

import (
	"github.com/justinas/alice"
	"bitbucket.org/opay-team/ng-hackathon-team-1/controllers"
	"github.com/gorilla/context"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"net/http"
	"os"
)

func InitRoutes() http.Handler {
	router := mux.NewRouter().StrictSlash(false)

	commonHandlers := alice.New(context.ClearHandler)

	router.Handle("/action", commonHandlers.ThenFunc(controllers.Action)).Methods(http.MethodPost)

	loggedRouter := handlers.LoggingHandler(os.Stdout, router)
	recoverableRouter := handlers.RecoveryHandler()(loggedRouter)
	return recoverableRouter
}
