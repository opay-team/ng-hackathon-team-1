package models

import (
	"errors"
	"gopkg.in/mgo.v2/bson"
	"time"
)

//---------------
// Typed error

var (
	ErrPhoneNumberInUse     = errors.New("the selected phone number is in use")
	ErrPinTooShort          = errors.New("pin too short")
	ErrInvalidPin          = errors.New("pin invalid")
	ErrRequiredFieldNotSent = errors.New("required field not sent")
	ErrUserNotFound = errors.New("the user was not found")
	ErrSenderNotRegister = errors.New("sender not registered")
	ErrInsufficientFund = errors.New("Account not funded")
)

type User struct {
	Id               bson.ObjectId `bson:"_id,omitempty" bson:"id"`
	Name             string        `bson:"name, omitempty"`
	PhoneNumber      string        `bson:"phone_number"`
	Balance          float64       `bson:"balance,omitempty"`
	Pin              string           `bson:"pin,omitempty"`
	RegistrationDate time.Time     `bson:"registration_date"`
}
