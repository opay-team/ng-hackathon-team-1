package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type TrxLog struct {
	Id          bson.ObjectId `json:"_id,omitempty" json:"id"`
	PhoneNumber string        `json:"phone_number"`
	Narration   string        `json:"narration"`
	Time        time.Time     `json:"time,omitempty"`
	Action      string        `json:"action,omitempty"`
	Raw         string        `json:"raw"`
}
