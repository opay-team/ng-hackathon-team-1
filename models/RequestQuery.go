package models


type RequestQuery struct {
	Phone string `json:"phone"`
	Code string `json:"code"`
}
