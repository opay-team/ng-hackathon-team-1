package service

import (
	"bitbucket.org/opay-team/ng-hackathon-team-1/data"
	"bitbucket.org/opay-team/ng-hackathon-team-1/models"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"strings"
	"time"
)

func CreateAccount(phoneNumber string, pin string) error {
	if len(strings.Trim(phoneNumber, " ")) == 0 ||
		len(strings.Trim(pin, " ")) == 0 {
			return models.ErrRequiredFieldNotSent
	}
	accountDao := data.NewDao(data.NewDb(), "users")

	if accountDao.Exists(bson.M{"phoneNumber": phoneNumber}) {
		return models.ErrPhoneNumberInUse
	}

	if len(strings.Trim(pin, " ")) < 4 {
		return models.ErrPinTooShort
	}

	account := models.User{
		Id: bson.NewObjectId(),
		PhoneNumber: phoneNumber,
		Pin:pin,
		RegistrationDate: time.Now(),
	}

	return accountDao.Insert(&account)
}

func CheckBalance(phoneNumber string, pin string) (float64, error) {
	accountDao := data.NewDao(data.NewDb(), "users")

	if len(strings.Trim(pin, " ")) < 4 {
		return 0,models.ErrPinTooShort
	}

	user := models.User{}
	err :=  accountDao.FindOne(bson.M{"phoneNumber": phoneNumber}, &user)
	if err != nil{
		return 0, models.ErrUserNotFound
	}

	if user.Pin != pin {
		return 0, models.ErrInvalidPin
	}

	return user.Balance, nil
}

type SendMoneyInput struct {
	Sender string
	Receiver string
	Amount float64
	Pin string
}

func SendMoney(input SendMoneyInput) error {
	accountDao := data.NewDao(data.NewDb(), "users")

	var sender models.User
	err := accountDao.FindOne(bson.M{"phoneNumber": input.Sender}, &sender)
	if err != nil {
		return models.ErrSenderNotRegister
	}

	var receiver models.User
	err = accountDao.FindOne(bson.M{"phoneNumber": input.Receiver}, &receiver)
	if err != nil {
		return models.ErrUserNotFound
	}

	if sender.Balance < input.Amount {
		return models.ErrInsufficientFund
	}

	receiver.Balance += input.Amount
	err = accountDao.Update(bson.M{"_id": sender.Id}, sender)
	if err != nil {
		return err
	}

	txDao := data.NewDao(data.NewDb(), "tx_log")

	tx := models.TrxLog{
		Id: bson.NewObjectId(),
		PhoneNumber: sender.PhoneNumber,
		Time: time.Now(),
		Action: "SEND MONEY",
		Narration: fmt.Sprintf("Sent %v to %s", input.Amount, receiver.PhoneNumber),
	}
	txDao.Insert(tx)


	sender.Balance -= input.Amount
	accountDao.Update(bson.M{"_id": sender.Id}, receiver)


	rTx := models.TrxLog{
		Id: bson.NewObjectId(),
		PhoneNumber: receiver.PhoneNumber,
		Time: time.Now(),
		Action: "RECEIVED MONEY",
		Narration: fmt.Sprintf("Received %v from %s", input.Amount, receiver.PhoneNumber),
	}
	txDao.Insert(rTx)

	return err
}