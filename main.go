package main

import (
	"bitbucket.org/opay-team/ng-hackathon-team-1/common"
	"bitbucket.org/opay-team/ng-hackathon-team-1/routers"
	"log"
	"net/http"
)

func main() {
	common.StartUp()

	router := routers.InitRoutes()

	server := &http.Server{
		Addr:    common.AppConfig.Server,
		Handler: router,
	}

	log.Println("Listening...")
	server.ListenAndServe()
}
